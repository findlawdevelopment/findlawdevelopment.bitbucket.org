var pathlib = require('path'),
    gulp = require('gulp'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload'),
    notify = require('gulp-notify'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer')
;

gulp.task('css', function () {
    return gulp.src('css/main.scss')
        .pipe(sass({
            onError: function (error) {
                return notify({
                    title: 'SCSS error',
                    message: error.message,
                    icon: pathlib.join(__dirname, 'img', 'scss-error.png')
                }).write(error);
            }
        }))
        .pipe(rename('main.css'))
        .pipe(autoprefixer())
        .pipe(gulp.dest('css'))
        .pipe(notify({
            title: 'SCSS compiled',
            icon: pathlib.join(__dirname, 'img', 'scss-success.png')
        }))
    ;
});



gulp.task('serve', ['css'], function () {
    var server = livereload.listen();

    console.log('watching sass files for changes');
    gulp.watch('css/**/*.scss', ['css']);


    gulp.watch('./css/main.css').on('change', livereload.changed);

    
});